package xyz.levert.huong.battleships.domain;

import java.io.Serializable;

public class Destroyer extends  AbstractShip implements Serializable {
    public Destroyer(Orientation orientation) {
        super("Destroyer", 'D', 2, orientation);
    }

    public Destroyer(){
        this(null);
    }
}
