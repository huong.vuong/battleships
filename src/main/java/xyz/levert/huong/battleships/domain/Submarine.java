package xyz.levert.huong.battleships.domain;

import java.io.Serializable;

public class Submarine extends AbstractShip implements Serializable {
    public Submarine(Orientation orientation) {
        super("Sub-marine", 'S', 3, orientation);
    }

    public Submarine() {
        this(null);
    }
}
