package xyz.levert.huong.battleships.domain;

import java.io.Serializable;

public class BattleShip extends AbstractShip implements Serializable {
    public BattleShip(Orientation orientation) {
        super("BattleShip", 'B', 4, orientation);
    }

    public BattleShip() {
        this(null);
    }

}

