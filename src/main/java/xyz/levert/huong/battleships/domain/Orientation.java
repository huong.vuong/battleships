package xyz.levert.huong.battleships.domain;

public enum Orientation {
    n("NORTH"),
    s("SOUTH"),
    e("EAST"),
    w("WEST");

    private final String o;

    Orientation(String o) {
        this.o = o;
    }

}
