package xyz.levert.huong.battleships.domain;

import xyz.levert.huong.battleships.manager.Board;
import xyz.levert.huong.battleships.manager.BoardException;
import xyz.levert.huong.battleships.manager.DistantPlayer;
import xyz.levert.huong.battleships.manager.InputHelper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Player {
    protected Board board;
    protected DistantPlayer opponent;
    protected int destroyedCount;
    protected AbstractShip[] ships;
    protected boolean lose;

    public Player(Board board, DistantPlayer opponent, List<AbstractShip> ships) {
        this.board = board;
        this.ships = ships.toArray(new AbstractShip[0]);
        this.opponent = opponent;
    }

    public Player(Board board, DistantPlayer opponent) {
        this.board = board;
        this.opponent = opponent;
    }

    /**
     * Read keyboard input to get ships coordinates. Place ships on given coodrinates.
     */
    public void putShips() throws BoardException {
        boolean done = false;
        int i = 0;
        InputHelper.ShipInput res;
        do {
            AbstractShip s = ships[i];
            do {
                String msg = String.format("placer %d : %s(%d)", i + 1, s.getName(), s.getLength());
                System.out.println(msg);
                res = InputHelper.readShipInput();

                //  set ship orientation
                Orientation o = Orientation.valueOf(res.orientation);
                s.setOrientation(o);
            } while (!canPutShip(s, res.x, res.y));

            board.putShip(s, res.x, res.y);
            board.print();
            ++i;
            done = i == 5;
        }
        while (!done);
    }

    public boolean sendHit() throws IOException {
        Hit hit;
        boolean isStrick = false;
        System.out.println("où frapper?");
        InputHelper.CoordInput hitInput;
        do {
            hitInput = InputHelper.readCoordInput();
        } while (hitInput.x >= board.getSize() || hitInput.y >= board.getSize());

        // call sendHit on this.opponentBoard
        // hit = opponentBoard.sendHit(hitInput.x, hitInput.y);

        while (board.hasHit(hitInput.x, hitInput.y)) {
            System.out.println("Retry ");
            hitInput = InputHelper.readCoordInput();
        }

        hit = opponent.sendHit(hitInput.x, hitInput.y);
        boolean won = false;
        switch (hit) {
            case STRIKE:
                isStrick = true;
                System.out.println(Hit.STRIKE.getLabel());
                break;
            case MISS:
                System.out.println(Hit.MISS.getLabel());
                break;
            case CARRIER:
                isStrick = true;
                System.out.println(Hit.CARRIER.getLabel());
                destroyedCount = destroyedCount + 1;
                break;
            case DESTROYER:
                isStrick = true;
                System.out.println(Hit.DESTROYER.getLabel());
                destroyedCount = destroyedCount + 1;
                break;
            case SUBMARINE:
                isStrick = true;
                System.out.println(Hit.SUBMARINE.getLabel());
                destroyedCount = destroyedCount + 1;
                break;
            case BATTLESHIP:
                isStrick = true;
                System.out.println(Hit.BATTLESHIP.getLabel());
                destroyedCount = destroyedCount + 1;
                break;
            case LOST:
                isStrick = true;
                System.out.println("Game won!");
                destroyedCount = destroyedCount + 1;
                won = true;
                break;
        }

        // Game expects sendHit to return BOTH hit result & hit coords.
        // return hit is obvious. But how to return coords at the same time ?
        board.setHit(isStrick, hitInput.x, hitInput.y);
        board.print();

        return won;
    }

    public boolean readHit() throws IOException {
        InputHelper.CoordInput hitInput = opponent.readHit();
        Hit hit = board.sendHit(hitInput.x, hitInput.y);

        boolean lost = Arrays.stream(ships)
                .allMatch(AbstractShip::isSunk);

        if (lost) {
            opponent.giveHitAnswer(Hit.LOST);
        } else {
            opponent.giveHitAnswer(hit);
        }

        return lost;
    }

    public int getDestroyedCount() {
        return destroyedCount;
    }

    private boolean canPutShip(AbstractShip ship, int x, int y) {
        Orientation o = ship.getOrientation();
        boolean res = false;
        for (int length = 0; length < ship.getLength(); length++) {
            if (x >= board.getSize() || x < 0 || y < 0 || y >= board.getSize() || board.hasShip(x, y)) {
                return false;
            } else {
                switch (o) {
                    case e:
                        x = x - 1;
                        break;
                    case n:
                        y = y + 1;
                        break;
                    case s:
                        y = y - 1;
                        break;
                    case w:
                        x = x + 1;
                        break;
                }
            }
        }
        return true;
    }

    public AbstractShip[] getShips() {
        return ships;
    }

    public void setShips(AbstractShip[] ships) {
        this.ships = ships;
    }
}

