package xyz.levert.huong.battleships.domain;


import java.io.Serializable;

public abstract class AbstractShip implements Serializable {
    private String name;
    private Character label;
    private int length;
    private Orientation orientation;
    private int strikeCount;

    public AbstractShip(String name, Character label, int length, Orientation orientation) {
        this.name = name;
        this.label = label;
        this.length = length;
        this.orientation = orientation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Character getLabel() {
        return label;
    }

    public void setLabel(Character label) {
        this.label = label;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public void addStrike() {
        if (!isSunk()) {
            strikeCount = strikeCount + 1;
        } else {
            throw new IllegalStateException();
        }
    }

    public boolean isSunk() {
        return strikeCount == length;
    }
}
