package xyz.levert.huong.battleships.domain;

import java.io.Serializable;

public class Carrier extends AbstractShip implements Serializable {
    public Carrier(Orientation orientation) {
        super("Carrier", 'C', 5, orientation);
    }
    public Carrier() {
        this(null);
    }
}
