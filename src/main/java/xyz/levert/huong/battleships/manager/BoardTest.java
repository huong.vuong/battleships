package xyz.levert.huong.battleships.manager;

import xyz.levert.huong.battleships.domain.*;

public class BoardTest {
    public static void main(String[] args) {

        Board board1 = new Board("Player 1", 10);
        Board board2 = new Board("Player 2", 10);
        boolean hit = false;
        boolean done = false;
        int i = 0;
        int j = 0;

        AbstractShip[] ships1 = {
                new Destroyer(), new Submarine(), new Submarine(), new BattleShip(), new Carrier()
        };

        AbstractShip[] ships2 = {
                new Destroyer(), new Submarine(), new Submarine(), new BattleShip(), new Carrier()
        };

        do {
            AbstractShip s1 = ships1[i];
            InputHelper.ShipInput res1 = InputHelper.readShipInput();

            // TODO Convert orientation
            Orientation orientation = Orientation.valueOf(res1.orientation);
            s1.setOrientation(orientation);

            try {
                board1.putShip(s1, res1.x, res1.y);
                ++i;
                done = i == 5;
            } catch (BoardException e) {
                System.err.println("Impossible de placer le navire a cette position");
            }
            board1.print();
        } while (!done);
/*
Board 2
*/
        do {
            AbstractShip s2 = ships2[j];
            InputHelper.ShipInput res2 = InputHelper.readShipInput();

            // TODO Convert orientation
            Orientation orientation = Orientation.valueOf(res2.orientation);
            s2.setOrientation(orientation);

            try {
                board2.putShip(s2, res2.x, res2.y);
                ++j;
                done = j == 5;
            } catch (BoardException e) {
                System.err.println("Impossible de placer le navire a cette position");
            }
            board2.print();
        } while (!done);

        board1.print();

        for (int race = 0; race < 2; race++) {
            InputHelper.CoordInput coordInput = InputHelper.readCoordInput();
            Boolean isPresent = board1.getHit(coordInput.x, coordInput.y);

            while (isPresent != null) {
                System.out.println("Retry ");
                coordInput = InputHelper.readCoordInput();
                isPresent = board1.getHit(coordInput.x, coordInput.y);
            }

            Hit resHit = board2.sendHit(coordInput.x, coordInput.y);

            switch (resHit) {
                case STRIKE:
                    hit = true;
                    break;
                case MISS:
                    System.out.println(Hit.MISS.getLabel());
                    break;
                case CARRIER:
                    hit = true;
                    System.out.println(Hit.CARRIER.getLabel());
                    break;
                case DESTROYER:
                    hit = true;
                    System.out.println(Hit.DESTROYER.getLabel());
                    break;
                case SUBMARINE:
                    hit = true;
                    System.out.println(Hit.SUBMARINE.getLabel());
                    break;
                case BATTLESHIP:
                    hit = true;
                    System.out.println(Hit.BATTLESHIP.getLabel());
                    break;
            }
            board1.setHit(hit, coordInput.x, coordInput.y);
            board1.print();
            board2.print();
        }
    }
}
