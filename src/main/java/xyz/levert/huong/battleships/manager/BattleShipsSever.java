package xyz.levert.huong.battleships.manager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class BattleShipsSever {

    private int port;

    private int nbShips;

    public BattleShipsSever(int port, int nbShips) {
        this.port = port;
        this.nbShips = nbShips;
    }

    public DistantPlayer listen() throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            Socket clientSocket = serverSocket.accept();
            DistantPlayer opponent = new DistantPlayer(clientSocket);
            opponent.giveNbShips(nbShips);
            return opponent;
        }
    }
}
