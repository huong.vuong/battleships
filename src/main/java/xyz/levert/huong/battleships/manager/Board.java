package xyz.levert.huong.battleships.manager;

import xyz.levert.huong.battleships.domain.*;

import java.io.*;

public class Board implements IBoard, Serializable {
    private String name;
    private ShipState[][] ships;
    private Boolean[][] hits;
    private int size;

    public Board(String name, int size) {
        this.name = name;
        this.size = size;
        hits = new Boolean[size][size];
        ships = new ShipState[size][size];
    }

    public Board(String name) {
        this(name, 10);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public ShipState getShips(int x, int y) {
        return ships[y][x];
    }

    public void setShips(ShipState[][] ships) {
        this.ships = ships;
    }

    @Override
    public void putShip(AbstractShip ship, int c, int r) throws BoardException {
        Orientation o = ship.getOrientation();
        for (int length = 0; length < ship.getLength(); length++) {
            ships[r][c] = new ShipState(ship);
            switch (o) {
                case e:
                    c = c - 1;
                    break;
                case n:
                    r = r + 1;
                    break;
                case s:
                    r = r - 1;
                    break;
                case w:
                    c = c + 1;
                    break;
            }
        }
    }

    @Override
    public boolean hasShip(int x, int y) {
        return ships[y][x] != null;
    }

    @Override
    public void setHit(boolean hit, int x, int y) {
        this.hits[y][x] = hit;
    }

    @Override
    public Boolean getHit(int x, int y) {
        return this.hits[y][x];
    }

    @Override
    public boolean hasHit(int x, int y) {
        return this.hits[y][x] != null;
    }

    @Override
    public Hit sendHit(int x, int y) {
        Hit h = null;
        if (hasShip(x, y)) {
            ShipState shipState = getShips(x, y);
            shipState.addStrike();
            if (shipState.isSunk()) {



                AbstractShip ship = shipState.getShip();
                if (ship instanceof Destroyer) {
                    h = Hit.DESTROYER;
                } else if (ship instanceof Submarine) {
                    h = Hit.SUBMARINE;
                } else if (ship instanceof BattleShip) {
                    h = Hit.BATTLESHIP;
                } else if (ship instanceof Carrier) {
                    h = Hit.CARRIER;
                }
            } else {
                h = Hit.STRIKE;
            }
        } else {
            h = Hit.MISS;
        }
        return h;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void print() {
        try {
            Runtime.getRuntime().exec("clear");
        } catch (IOException e) {
        }

        int screenSize = size * 4 + 5;

        /*
         * Print first line
         * */
        System.out.println(name);
        String firstLine = "Navires: Frappes:";
        firstLine = String.format("Navires: %" + (screenSize - firstLine.length()) / 2 + "s Frappes:", "");

        System.out.println(firstLine);

        /*
         * Print second line
         * */
        char currentChar = 'A';
        System.out.print("  ");

        for (int index = 0; index < size; index++) {
            System.out.print(currentChar + " ");
            currentChar++;

        }
        System.out.print("    ");

        currentChar = 'A';
        for (int index = 0; index < size; index++) {
            System.out.print(currentChar + " ");
            currentChar++;
        }
        System.out.println();

        /*
         * Print table
         * */

        for (int row = 0; row < size; row++) {

            System.out.printf("%2d", (row + 1));

            for (int column = 0; column < size; column++) {
                if (!hasShip(column, row)) {
                    System.out.print(". ");
                } else {
                    System.out.print(getShips(column, row).toString() + " ");
                }
            }
            System.out.print("  ");

            System.out.printf("%2d", (row + 1));

            for (int col = 0; col < size; col++) {

                if (getHit(col, row) != null) {
                    if (getHit(col, row)) {
                        System.out.print(ColorUtil.colorize("X ", ColorUtil.Color.RED));
                    } else {
                        System.out.print(ColorUtil.colorize("X ", ColorUtil.Color.WHITE));
                    }
                } else {
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }

    public static void saveBoard(Board board, File saveFile) {
        try {
            if (!saveFile.exists()) {
                saveFile.getAbsoluteFile().getParentFile().mkdir();
            }
            FileOutputStream out = new FileOutputStream(saveFile);
            ObjectOutputStream oout = new ObjectOutputStream(out);
            oout.writeObject(board);
            oout.flush();
            oout.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Board loadBoard(File saveFile) {
        Board board = null;

        if (saveFile.exists()) {
            try {
                ObjectInputStream obj = new ObjectInputStream((new FileInputStream(saveFile)));
                board = (Board) obj.readObject();
                obj.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return board;
    }
}
