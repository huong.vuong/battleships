package xyz.levert.huong.battleships.manager;

import xyz.levert.huong.battleships.domain.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.util.*;

public class Game {

    public static void main(String[] args) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Random random = new Random();
        Board playerBoard;
        Player player;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Choice your role:");
        System.out.println("1. Server");
        System.out.println("2. Client");

        int choice = Integer.parseInt(scanner.nextLine());

        DistantPlayer opponent;

        int nbShips;

        switch (choice) {
            case 1:
                System.out.println("Enter your port: ");
                int clientPort = Integer.parseInt(scanner.nextLine());
                nbShips = random.nextInt(10) + 1;
                BattleShipsSever server = new BattleShipsSever(clientPort, nbShips);
                opponent = server.listen();
                break;
            case 2:
                System.out.println("Enter your severIP and severPort : ");
                String[] input = scanner.nextLine().split(" ");
                String severIP = input[0];
                int severPort = Integer.parseInt(input[1]);
                opponent = new DistantPlayer(new Socket(severIP, severPort));
                nbShips = opponent.readNbShips();
                break;
            default:
                throw new IllegalArgumentException();
        }

        System.out.println("Enter your name :");
        playerBoard = new Board(scanner.nextLine(), nbShips);

        List<AbstractShip> playerShips = new ArrayList<>();
        List<Class<? extends AbstractShip>> classes = Arrays.asList(BattleShip.class, Carrier.class, Destroyer.class, Submarine.class);

        for (int i = 0; i < nbShips; i++) {
            int randomIn = random.nextInt(classes.size());
            Class<? extends AbstractShip> clazz = classes.get(randomIn);

            playerShips.add(
                    clazz.getDeclaredConstructor().newInstance()
            );
        }

        player = new Player(playerBoard, opponent, playerShips);

        // Put the ships into playerBoard
        try {
            player.putShips();
            playerBoard.print();
        } catch (BoardException e) {
            e.printStackTrace();
        }

        int turn = 0;
        boolean done;

        do {
            if (((turn + choice) % 2) == 0) {
                // play first
                done = player.sendHit();
            } else {
                // wait first
                done = player.readHit();
            }
            playerBoard.print();
            turn++;
        } while (!done);

        opponent.close();
    }
}
