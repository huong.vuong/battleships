package xyz.levert.huong.battleships.manager;

import xyz.levert.huong.battleships.domain.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TestGame {
    public static void main(String[] args) {
        Board myBoard = new Board("Player 1", 10);
        Board opponentBoard = new Board("Player 2", 10);
        List<AbstractShip> myShipsTemp = Arrays.asList(new Destroyer(), new Submarine(), new Submarine(), new BattleShip(), new Carrier());
        List<AbstractShip> myShips = new ArrayList<>();
        myShips.addAll(myShipsTemp);

        List<AbstractShip> opponentShipsTemp = Arrays.asList(new Destroyer(), new Submarine(), new Submarine(), new BattleShip(), new Carrier());
        List<AbstractShip> opponentShips = new ArrayList<>();
        opponentShips.addAll(opponentShipsTemp);

        BattleShipsIA myIA = new BattleShipsIA(myBoard, opponentBoard);
        BattleShipsIA opponentIA = new BattleShipsIA(opponentBoard, opponentBoard);
        int shipCount = 0;
        boolean done = false;
        boolean hit = false;

//        Put the ships into Board 1
        try {
            myIA.putShips(myShips);
            myBoard.print();
        } catch (BoardException e) {
            e.printStackTrace();
        }

//        Put ships into opponentBoard

        try {
            opponentIA.putShips(opponentShips);
            opponentBoard.print();
        } catch (BoardException e) {
            e.printStackTrace();
        }

        while (shipCount < opponentShips.size()) {
            Random rnd = new Random();
            int xHit, yHit;

            xHit = rnd.nextInt(opponentShips.size());
            yHit = rnd.nextInt(opponentShips.size());

            Boolean isPresent = myBoard.getHit(xHit, yHit);

            while (isPresent != null) {
                System.out.println("Retry ");
                xHit = rnd.nextInt(myBoard.getSize());
                yHit = rnd.nextInt(myBoard.getSize());
                isPresent = myBoard.getHit(xHit, yHit);
            }

            Hit resHit = opponentBoard.sendHit(xHit, yHit);

            switch (resHit) {
                case STRIKE:
                    hit = true;
                    System.out.println("touché !");
                    break;
                case MISS:
                    hit = false;
                    System.out.println("maqué !");
                    break;
                case CARRIER:
                    hit = true;
                    System.out.println(Hit.CARRIER.getLabel() + " coulé");
                    shipCount = shipCount + 1;
                    break;
                case DESTROYER:
                    hit = true;
                    System.out.println(Hit.DESTROYER.getLabel() + " coulé");
                    shipCount = shipCount + 1;
                    break;
                case SUBMARINE:
                    hit = true;
                    System.out.println(Hit.SUBMARINE.getLabel() + " coulé");
                    shipCount = shipCount + 1;
                    break;
                case BATTLESHIP:
                    hit = true;
                    System.out.println(Hit.BATTLESHIP.getLabel() + " coulé");
                    shipCount = shipCount + 1;
                    break;
            }
            myBoard.setHit(hit, xHit, yHit);
            myBoard.print();
            opponentBoard.print();
        }
    }
}
