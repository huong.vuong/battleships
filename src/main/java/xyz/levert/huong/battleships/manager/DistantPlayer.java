package xyz.levert.huong.battleships.manager;

import xyz.levert.huong.battleships.domain.Hit;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class DistantPlayer implements Closeable {

    private final Socket socket;

    private final Writer writer;

    private final BufferedReader reader;

    public DistantPlayer(Socket socket) throws IOException {
        this.socket = socket;
        writer = new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
    }

    @Override
    public void close() throws IOException {
        writer.close();
        reader.close();
        socket.close();
    }

    public Hit sendHit(int x, int y) throws IOException {
        writer.write(String.format("%d %d", x, y));
        writer.write('\n');
        writer.flush();

        String line = reader.readLine();
        return Hit.valueOf(line);
    }

    public InputHelper.CoordInput readHit() throws IOException {
        String line = reader.readLine();
        String[] coord = line.split(" ");
        InputHelper.CoordInput res = new InputHelper.CoordInput();
        res.x = Integer.parseInt(coord[0]);
        res.y = Integer.parseInt(coord[1]);
        return res;
    }

    public void giveNbShips(int nbShips) throws IOException {
        writer.write(Integer.toString(nbShips));
        writer.write('\n');
        writer.flush();
    }

    public int readNbShips() throws IOException {
        return Integer.parseInt(reader.readLine());
    }

    public void giveHitAnswer(Hit hit) throws IOException {
        writer.write(hit.name());
        writer.write('\n');
        writer.flush();
    }
}
