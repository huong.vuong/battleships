package xyz.levert.huong.battleships.manager;

import xyz.levert.huong.battleships.domain.AbstractShip;
import xyz.levert.huong.battleships.domain.ColorUtil;

import java.io.Serializable;

public class ShipState implements Serializable {

    private AbstractShip ship;

    private boolean struck;

    public ShipState(AbstractShip ship) {
        this.struck = false;
        this.ship = ship;
    }

    public void addStrike() {
        this.struck = true;
        ship.addStrike();
    }

    public boolean isStruck() {
        return struck;
    }

    public boolean isSunk() {
        return ship.isSunk();
    }

    public AbstractShip getShip() {
        return ship;
    }

    @Override
    public String toString() {
        if (isStruck()) {
            return ColorUtil.colorize(ship.getLabel().toString(), ColorUtil.Color.RED);
        } else {
            return ColorUtil.colorize(ship.getLabel().toString(), ColorUtil.Color.WHITE);
        }
    }
}
